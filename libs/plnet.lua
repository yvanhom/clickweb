local http = require 'resty.http'
local gumbo = require 'gumbo'
local util = require("lapis.util")
local json = require "cjson"

local root = "http://www.proxylists.net"
local updating = false
local proxies = nil
local filepath = "./proxies.json"

local function getCountries()
  local url = root .. "/countries.html"

  local httpc = http.new()
  local res, err = httpc:request_uri(url)
  if not res then
    return nil, "Can't get country list: " .. err
  end
  if tonumber(res.status) ~= 200 then
    print(res.body)
    return nil, "Error status: " .. err
  end

  local rnode = gumbo.parse(res.body)
  local tables = rnode:getElementsByTagName("table")
  tables = tables and #tables > 0 and tables[1]:getElementsByTagName("table")
  local links = tables and #tables > 0 and tables[1]:getElementsByTagName("a")
  if not links or #links == 0 then
    print(res.body)
    return nil, "Unknown table format"
  end
  local ret = {}
  for i, link in ipairs(links) do
    local t = {}
    local href = link:getAttribute("href")
    t.name = link.innerHTML
    t.short = href and string.match(href, "/(.*)_0.html")
    if t.short and t.name then
      table.insert(ret, t)
    end
  end
  httpc:set_keepalive()
  return ret
end

local function updateCountryProxies(c, ps)
  local index = 0
  local httpc = http.new()
  local hasmore = true
  while hasmore do
    local url = string.format("%s/%s_%d_ext.html", root, c.short, index)
    print(url)
    local res, err = httpc:request_uri(url)
    if not res then
      return nil, "Can't request: " .. err
    end
    if res.status ~= 200 then
      print(res.body)
      return nil, "Error status: " .. res.status
    end
    local rnode = gumbo.parse(res.body)
    local tables = rnode:getElementsByTagName("table")
    tables = tables and #tables > 0 and tables[1]:getElementsByTagName("table")
    local trs = tables and #tables > 0 and tables[1]:getElementsByTagName("tr")
    hasmore = false
    if trs then
      for _, tr in ipairs(trs) do
        local script = tr:getElementsByTagName("script")
        local tds = tr:getElementsByTagName("td");
        if script and #script > 0 and #tds >= 4 then
          local p = {}
          p.ip = string.match(script[1].innerHTML, "'([^']+)'")
          p.ip = p.ip and util.unescape(p.ip)
          p.ip = p.ip and string.match(p.ip, "\"([0-9%.]+)\"")
          p.port = tds[2].innerHTML
          p.type = tds[3].innerHTML
          p.country = tds[4].innerHTML
          print(string.format("PROXY: %s, %s, %s, %s", p.ip, p.port, p.type, p.country))
          hasmore = true
          table.insert(ps, p)
        end
      end
    end

    index = index + 1
  end
end

local function loadFromFile(path)
  local file, err = io.open(path, "r")
  if not file then
    return nil, "Update first: " .. err
  end
  local s = file:read("*a")
  file:close()

  local t = json.decode(s)
  return t
end

local function saveToFile(path, t)
  local file, err = io.open(path, "w")
  if not file then
    return nil, "Can't save the file: " .. err
  end
  local s = json.encode(t)
  file:write(s)
  file:close()
  return true
end

local function updateProxies()
  if updating then 
    return nil, "Updating ..."
  end

  updating = true
  proxies = {}
  local cs, err = getCountries()
  if not cs then
    updating = false
    return nil, err
  end

  for _, c in ipairs(cs) do
    updateCountryProxies(c, proxies)
  end

  saveToFile(filepath, proxies)
  updating = false
end

local function getProxies()
  if updating then
    return nil, "Updating..."
  end
  if not proxies then
    local ps, err = loadFromFile(filepath)
    if not ps then 
      return nil, err
    end
    proxies = ps
  end
  return proxies
end

local function accessThroughProxy(proxy, url)
  if proxy.type ~= 'Transparent' and proxy.type ~= 'Anonymous' then
    return nil, "Unsupported proxy: " .. proxy.type
  end
  local httpc = http.new()
  httpc:set_timeout(5000)
  local ok, err = httpc:connect(proxy.ip, tonumber(proxy.port))
  if not ok then
    return nil, "Can't connect: " .. err
  end
  local res, err = httpc:request({
    method = "GET",
    path = url,
  })
  if not res then
    return nil, "Can't request: " .. err
  end
  local body, err = res:read_body()
  if tonumber(res.status) ~= 200 then
    print(body)
    return nil, "Error status: " .. res.status
  end

  httpc:close()
  return body
end

return {
  updateProxies = updateProxies,
  getProxies = getProxies,
  accessThroughProxy = accessThroughProxy,
}

local lapis = require("lapis")
local app = lapis.Application()

app:enable("etlua")
app.layout = require "views.layout"

app:get("index", "/", function()
  return { render = "index" }
end)

app:post("click", "/click", function(self)
  return require("controls.click")(self)
end)

app:get("update", "/update", function(self)
  return require("controls.update")(self)
end)

app:post("translate", "/translate/tr_:tr", function(self)
  return require("controls.translate")(self)
end)

return app

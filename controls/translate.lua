local mstr = require "libs.mstr"

local function translate(req)
  local tr = req.params.tr
  local src = req.params.src

  local dest, err = mstr.translate(tr, "cn", src)
  if not dest then
    return { json = { status = "FAIL", errmsg = err }}
  end

  return { json = { status = "SUCCESS", dest = dest }}
end

return translate

local plnet = require 'libs.plnet'

local function update(req)
  plnet.updateProxies()
  req.msg = "Update successfully!"

  return { render = "index" }
end

return update

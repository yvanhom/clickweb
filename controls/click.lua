local plnet = require 'libs.plnet'
local basexx = require 'basexx'
local encoding = require 'lapis.util.encoding'

local function click(req)
  local url = req.params.url
  req.msg = "clicking " .. url .. " ..."

  local ps, err = plnet.getProxies()
  if not ps then
    req.errmsg = err
    return { render = "index" }
  end

  local filename = basexx.to_url64(encoding.hmac_sha1("aaaBBB", url)) .. ".txt"

  ngx.thread.spawn(function()
    local out = io.open("cache/" .. filename, "w")
    local sc = 0
    for i, p in ipairs(ps) do
      local body, err = plnet.accessThroughProxy(p, url)
      if not body then
         out:write(string.format("%d: %s %s %s %s, ERROR: %s", 
           i, p.ip, p.port, p.type, p.country, err))
      else
         out:write(string.format("%d: %s %s %s %s, SUCCESS",
           i, p.ip, p.port, p.type, p.country))
         sc = sc + 1
      end
      out:write("\n")
    end
    out:write(string.format("%d: %d / %d", -1, sc, #ps))
    out:write("\n")
    out:close()
  end)

  req.progress = filename

  return { render = "index" }
end

return click

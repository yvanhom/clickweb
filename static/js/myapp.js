$(function() {
  $('button[id^=tr_]').click(function() {
    var src = $('#' + this.id + '_src')
    var dest = $('#' + this.id + '_dest')
    $.ajax({
      url: "/translate/" + this.id,
      type: "POST",
      data: {
        src: src.val()
      },
      success: function(result) {
        if(result.status === "SUCCESS") {
          dest.val(result.dest)
        }
      },
      fail: function(result) {}
    })
  })
})
